# catophony

## What is it

A small generative art project based on https://observablehq.com/@saneef/062-mugshots

Supposedly random cats depict intricate interaction between cells of different types in the bone marrow
![blinking cats](./images/animation_all_eyes.gif)

## How to run

` python3 -m http.server 8001 `

Open the page in browser `http://0.0.0.0:8001/` and choose the directory with the project

Then the page will suggest to save the image

Appearance of the cats is controled by a random seed in `scripts/cat.js`:  

`let randomize = 47;`  

You can change it, update the page in the browser, get new image, yay  


## Making GIF

Number of images generated is controlled by a variable in `scripts/cat.js`:

`let number = 1;`

Set it to the desired number of frames, then for the same cat faces several images wth randomized open/closed eyes will be generated. Then you need to save them manually (more or less just click "Save" several times so it's ok)

Convert to png

`for i in *.svg; do convert -background none MSVG:$i  ../pngs/${i%%.*}.png ; done`

Make GIF  
`convert -delay 20 -loop 0 *.png animation.gif`