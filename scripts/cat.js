import { svg } from "https://esm.sh/htl@0.3.1";
import {random, math as m, color} from "https://esm.sh/canvas-sketch-util";
import * as chromotome from "https://unpkg.com/chromotome@1.20.0/dist/index.esm.js?module"
import * as d3 from  "https://esm.sh/d3";


function generateFace({
    debug = false,
    width = 150,
    height = 200,
    features = ["mouth", "eyes", "nose", "hair"],
  
    fill = "hsl(0,0%,95%)",
    stroke = "hsl(0,0%,5%)",
    strokeWidth = 1.5,
    strokeLinejoin = "round",
    strokeLinecap = "round",
  
    noseWidth = 1,
    earRatio = 0.2,
  
    formity = 0.5,
    happiness = 0,
  
    hairStyle = "flat",
    hairVariation = 0.5,

    earDirection = 0
  } = {}) {
    const aspectRatio = width / height;
    const happinessUV = happiness * 0.5 + 0.5;
  
    const earHeight = height * earRatio;
    const earWidth = width/ 4;
    const earEndPos = earDirection * earWidth;
    const faceHeight = height - earHeight;

    const verticalDivisions = 3;
    const verticalDivHeight = faceHeight / verticalDivisions;
  
    const eyelineDivisions = 5;
    const eyelineY = earHeight + verticalDivHeight + (verticalDivHeight * 1) / 4;
    const eyeWidth = width / eyelineDivisions;
  
    
  
    const tickSize = 10;
    
    const noseSize = 0.5;
    const faceWidth = width;

    const noseUp = earHeight + eyelineY;
    const noseDown = earHeight + eyelineY + 0.05 * height;

    const mouthLine = noseDown + (height - noseDown) / 3;
    const mouthEdge = mouthLine + happiness * (height - noseDown) / 3;

    const eye_rnd = random.createRandom();
    let eyeOpen = eye_rnd.weightedSet([
      { value: true, weight: 6 },
      { value: false, weight: 1 }
    ]);

  
    const drawSkin = () => {

      const dCx = (formity * faceWidth) / 5;
  
      return svg`<g transform="translate(${0},${earHeight})">
    <path
     d="M${faceWidth / 2},0
        Q${faceWidth},0 ${faceWidth},${faceHeight / 2}
        Q${faceWidth - dCx},${faceHeight} ${faceWidth / 2},${faceHeight}
        Q${dCx},${faceHeight} 0,${faceHeight / 2}
        Q0,0 ${faceWidth / 2},0
        Z"
      fill=${fill}
      stroke=${fill}
    ></path>
  </g>`;
    };
  
    const drawEars = () => {
      const d = `M${earEndPos},${0}
      L${0},${faceHeight / 2 + earHeight}
      L${width / 2},${faceHeight / 2 + earHeight}
      Z`;
  
      return svg`<g class="ears">
      <path
       d=${d}
       fill=${fill}
      ></path>
      <g transform="scale(-1,1) translate(${-width},${0}) ">
        <path
         d=${d}
         fill=${fill}
         stroke="none"
        ></path>
      </g>
    </g>`;
    };
  
    const drawEyes = () => {
      if (!features.includes("eyes")) return;
  
      const eyeY = eyelineY;
      const eyeXFromMid = (eyeWidth * 4) / 4;
      const eyeDx = m.lerp(0, eyeWidth / 3, happinessUV);
      const eyeStrokeWidth = m.lerp(strokeWidth * 2, strokeWidth, happinessUV);
      const eyeDy = eyeDx
      
      if (eyeOpen) {
      return svg`<g class="eyes"> 
    <path
    d="M${width / 2 - eyeXFromMid - eyeDx},${eyeY}
       A${eyeDx},${happiness * 0.8 * eyeDy} ${0} ${1} ${1} ${width / 2 - eyeXFromMid + eyeDx},${eyeY}
       A${eyeDx},${0.8 * eyeDy} ${0} ${1} ${1} ${width / 2 - eyeXFromMid - eyeDx},${eyeY}
       Z"
    r=20
    fill=${stroke}
    stroke=${stroke}
    stroke-width=${strokeWidth}
    stroke-linejoin=${strokeLinejoin}
    stroke-linecap=${strokeLinecap}
    ></path>

    <path
    d="M${width / 2 + eyeXFromMid - eyeDx},${eyeY}
      A${eyeDx},${happiness * 0.8 * eyeDy} ${0} ${1} ${1} ${width / 2 + eyeXFromMid + eyeDx},${eyeY}
      A${eyeDx},${0.8 * eyeDy} ${0} ${1} ${1} ${width / 2 + eyeXFromMid - eyeDx},${eyeY}
        Z"
    r=20
    fill=${stroke}
    stroke=${stroke}
    stroke-width=${strokeWidth}
    stroke-linejoin=${strokeLinejoin}
    stroke-linecap=${strokeLinecap}
    ></path>
  </g>`;
    }
    else {
      return svg`<g class="eyes"> 
    <path
    d="M${width / 2 - eyeXFromMid - eyeDx},${eyeY}
       L${width / 2 - eyeXFromMid + eyeDx},${eyeY}
       Z"
    r=20
    fill=${stroke}
    stroke=${stroke}
    stroke-width=${strokeWidth}
    stroke-linejoin=${strokeLinejoin}
    stroke-linecap=${strokeLinecap}
    ></path>

    <path
    d="M${width / 2 + eyeXFromMid - eyeDx},${eyeY}
       L${width / 2 + eyeXFromMid + eyeDx},${eyeY}
        Z"
    r=20
    fill=${stroke}
    stroke=${stroke}
    stroke-width=${strokeWidth}
    stroke-linejoin=${strokeLinejoin}
    stroke-linecap=${strokeLinecap}
    ></path>
  </g>`;
    }
  };
  
    const drawNose = () => {
      if (!features.includes("nose")) return;
  
      const noseWidth = eyeWidth * noseSize;

  
      return svg`<g class="nose">
    <path
    d="M${faceWidth / 2 - noseWidth},${noseUp}
       L${faceWidth / 2 + noseWidth},${noseUp}
       L${faceWidth / 2},${noseDown}
       Z"
    r=20
    fill=${stroke}
    stroke=${stroke}
    stroke-width=${strokeWidth}
    stroke-linejoin=${strokeLinejoin}
    stroke-linecap=${strokeLinecap}
    ></path>
  </g>`;
    };
  
    const drawMouth = () => {
      if (!features.includes("mouth")) return;
  
      const cx = width / 2;
      const mouthY = mouthLine;
      const mouthDy =
        happiness < 0
          ? m.lerp(0, -verticalDivHeight / 3, Math.abs(happiness))
          : m.lerp(0, verticalDivHeight / 2, Math.abs(happiness));
      const mouthDx = m.lerp(eyeWidth * 0.25, eyeWidth * 0.85, happinessUV);
      return svg` <path
      class="mouth"
      d="M${cx},${noseDown} 
         L${cx},${mouthLine}"
      r=20
      fill="none"
      stroke=${stroke}
      stroke-width=${strokeWidth}
      stroke-linejoin=${strokeLinejoin}
      stroke-linecap=${strokeLinecap}
      ></path>
      <path
      class="mouth"
      d="M${cx - mouthDx},${mouthLine} 
          Q${cx - (happiness - 0.1) * mouthDx},${mouthEdge} ${cx},${mouthLine} "
      r=20
      fill="none"
      stroke=${stroke}
      stroke-width=${strokeWidth}
      stroke-linejoin=${strokeLinejoin}
      stroke-linecap=${strokeLinecap}
      ></path>
      <path
      class="mouth"
      d="M${cx + mouthDx},${mouthLine} 
          Q${cx + (happiness - 0.1) * mouthDx},${mouthEdge} ${cx},${mouthLine} "
      r=20
      fill="none"
      stroke=${stroke}
      stroke-width=${strokeWidth}
      stroke-linejoin=${strokeLinejoin}
      stroke-linecap=${strokeLinecap}
      ></path>
      </g>`;
    };
  
    const drawHair = () => {
      if (!features.includes("hair")) return;
  
      if (hairStyle === "flat") {
        const hairY = m.lerp(
          verticalDivHeight * 0.25,
          verticalDivHeight * 0.75,
          hairVariation
        );
        return svg`<line class="hair"
          x1=${earWidth}
          y1=${hairY}
          x2=${width - earWidth}
          y2=${hairY}
          fill="none"
          stroke=${stroke}
          stroke-width=${strokeWidth}
          ></line>`;
      } else if (hairStyle === "parted") {
        const hairYMin = verticalDivHeight * 0.25;
        const hairPartitionX = m.lerp(
          earWidth * 3,
          width - earWidth * 3,
          hairVariation
        );
        return svg`<path class="hair"
      d="M${earWidth},${verticalDivHeight - strokeWidth / 2} 
         A ${hairPartitionX - earWidth}
           ${verticalDivHeight - hairYMin} 0 0 0 ${hairPartitionX},${hairYMin}
         A ${width - earWidth - hairPartitionX}
           ${verticalDivHeight - hairYMin}
           0 0 0 ${width - earWidth},${verticalDivHeight - strokeWidth / 2}"
      fill="none"
      stroke=${stroke}
      stroke-width=${strokeWidth}
      stroke-linejoin=${strokeLinejoin}
      stroke-linecap=${strokeLinecap}
    ></path>`;
      } else if (hairStyle === "wavy") {
        const hairYMin = m.lerp(
          verticalDivHeight * 0.8,
          verticalDivHeight * 0.9,
          hairVariation
        );
        const hairYMax = verticalDivHeight * 1.25 - strokeWidth / 2;
        const hairPartitionX = m.lerp(
          earWidth * 1.333,
          width - earWidth * 1.333,
          hairVariation
        );
        const partions = 2 * Math.floor(m.lerp(3, 6, hairVariation));
        const points = m
          .linspace(partions + 1, true)
          .map((u, i) => [
            earWidth + u * (width - 2 * earWidth),
            i % 2 === 0 ? hairYMin : hairYMax
          ]);
  
        const path = d3.path();
        points.forEach((p, i) => {
          if (i === 0) {
            path.moveTo(...p);
          } else if (i % 2 !== 0) {
          } else {
            const cx = points[i - 1][0];
            const cy = points[i - 1][1];
            path.quadraticCurveTo(cx, cy, ...p);
          }
        });
        const d = path.toString();
        return svg`<path class="hair"
      d=${d}
      fill="none"
      stroke=${stroke}
      stroke-width=${strokeWidth}
      stroke-linejoin=${strokeLinejoin}
      stroke-linecap=${strokeLinecap}
    ></path>`;
      }
    };
  
    let debugEls;
    if (debug) {
      debugEls = svg`<g class="debug">
    <rect width=${width} height=${height} stroke="#ff0" fill="none"></rect>
  ${d3.range(verticalDivisions - 1).map(
    (i) =>
      svg`<line
                    x1=0
                    y1=${(i + 1) * verticalDivHeight}
                    x2=${width}
                    y2=${(i + 1) * verticalDivHeight}
                    stroke="#f0f" 
                    stroke-width=${strokeWidth}
                    fill="none"></rect>`
  )}
    <line
      x1="0" y1=${eyelineY}
      x2=${width} y2=${eyelineY}
      stroke="#0ff"
      stroke-width=${strokeWidth}
      ></line>
  ${d3.range(eyelineDivisions + 1).map(
    (i) => svg`<line
    x1=${i * eyeWidth}
    y1=${eyelineY - tickSize / 2}
    x2=${i * eyeWidth}
    y2=${eyelineY + tickSize / 2}
    stroke="#0ff"
    stroke-width=${strokeWidth}></line>`
  )}
  <line
      x1=${eyeWidth * 1.5}
      y1=${mouthLine}
      x2=${width - eyeWidth * 1.5}
      y2=${mouthLine}
      stroke="#0ff"
      stroke-width=${strokeWidth}
      ></line>
  <path
    d="M${width / 2},${eyelineY} 
      L${eyeWidth * 2},${2 * verticalDivHeight}
      L${eyeWidth * 3},${2 * verticalDivHeight}z"
    fill="none"
    stroke="#0ff"
    stroke-width=${strokeWidth}
    ></path>
  </g>`;
    }
    return svg`<g class="face">
    ${drawEars()}
    ${drawSkin()}
    ${drawNose()}
    ${drawEyes()}
    ${drawMouth()}
    ${debugEls}
  </g>`;
  }


function download(filename, text) {
  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  pom.setAttribute('download', filename);

  if (document.createEvent) {
      var event = document.createEvent('MouseEvents');
      event.initEvent('click', true, true);
      pom.dispatchEvent(event);
  }
  else {
      pom.click();
  }
}

function generateFacesData(rnd, grid) {
  return m
    .linspace(grid.rows, true)
    .map((u) =>
      m.linspace(grid.cols, true).map((v) => {
        const color = rnd.weightedSet([
          {
            value: 0,
            weight: 1
          },
          {
            value: 1,
            weight: 1
          },
          {
            value: 2,
            weight: 1
          },
          {
            value: undefined,
            weight: 7
          }
        ]);

        let features = [];
        const formity = m.clamp01(rnd.gaussian(0.0125) * 0.5 + 0.5);

        // Hair
        const hairStyle = rnd.weightedSet([
          {
            value: "parted",
            weight: 1
          },
          {
            value: "flat",
            weight: 1
          },
          {
            value: "wavy",
            weight: 1
          },
          {
            value: undefined,
            weight: 6
          }
        ]);
        const hairVariation = m.clamp01(rnd.gaussian() * 0.5 + 0.5);
        if (hairStyle) {
          features.push("hair");
        }

        // Mouth
        const hasMouth = rnd.weightedSet([
          { value: true, weight: 3 },
          { value: false, weight: 1 }
        ]);
        const isHappy = rnd.weightedSet([
          { value: true, weight: 3 },
          { value: false, weight: 1 }
        ]);
        const happiness = m.clamp01(rnd.gaussian() * 0.5 + 0.5);

        if (hasMouth) {
          features.push("mouth");
        }

        // Eyes
        const haveEyes = rnd.weightedSet([
          { value: true, weight: 5 },
          { value: true, weight: 1 }
        ]);
        if (haveEyes) {
          features.push("eyes");
        }

        // Nose
        const hasNose = rnd.weightedSet([
          { value: true, weight: 6 },
          { value: false, weight: 1 }
        ]);
        const noseWidth = m.clamp01(rnd.gaussian() * 0.5 + 0.5);
        
        if (hasNose) {
          features.push("nose");
        }

        // Ear
        const earRatio = m.clamp01(rnd.gaussian() * 0.1 + 0.1) + 0.1;
        const earDirection = m.clamp01(rnd.gaussian() * 0.5 + 0.5);

        return {
          features,
          happiness,
          formity,
          noseWidth,
          earRatio,
          hairStyle,
          hairVariation,
          color,
          earDirection
        };
      })
    )
    .flat();
}

function getPalette(rnd) {
  // return {
  //     background: "hsl(0,0%,2.5%)",
  //     foreground: "hsl(0,0%,97.5%)",
  //     colors: [
  //       "hsl(195, 75%, 60%)", //Blue
  //       "hsl(55, 95%, 69%)", //Yellow
  //       "hsl(12, 75%, 60%)" //Red
  //     ]
  //   };
  const palettes = getAllPalettes();
  const p = rnd.pick(palettes);
  let { foreground, background } = p;

  if (
    color.relativeLuminance(background) > color.relativeLuminance(foreground)
  ) {
    return {
      ...p,
      foreground: background,
      background: foreground
    };
  }

  return p;
}

function generateIrregularGrid({
  baseGrid,
  baseWidth = 640,
  baseHeight = 480,
  cells = 90,
  random = CSRandom,
  freq = 15,
  amp = 0.75,
  padding = 0.5
} = {}) {
  const p = m.clamp01(padding);
  const paddingPx = (p * Math.min(baseWidth, baseHeight)) / 50;

  const grid = baseGrid ?? squaresInRectangle(baseWidth, baseHeight, cells);

  const { side, rows, cols } = grid;
  const gridWidth = side * cols - paddingPx;
  const gridHeight = side * rows - paddingPx;
  const maxDu = (side / (side * cols)) * 0.5;

  let generatedCells = m.linspace(rows, true).map((v, j) => {
    const heightRatio = random.noise2D(v / 10, 100, freq, amp) * 0.5 + 0.5;

    return m.linspace(cols, true).map((u, i) => {
      const widthRatio =
        random.noise3D(u / 10, v / 10, 0, freq, amp) * 0.5 + 0.5;

      return {
        widthRatio,
        heightRatio
      };
    });
  });

  const totalHeightRatio = d3.sum(generatedCells, (d) => d[0].heightRatio);
  const totalWidthRatios = generatedCells.map((row) => {
    return d3.sum(row, (d) => d.widthRatio);
  });

  generatedCells = generatedCells.map((row, j) => {
    return row.map((c, i) => {
      const width = (gridWidth * c.widthRatio) / totalWidthRatios[j];
      const height = (gridHeight * c.heightRatio) / totalHeightRatio;
      return {
        // ...c,
        width,
        height
      };
    });
  });

  generatedCells = generatedCells.reduce((accRows, row, j) => {
    const nextRow = row.reduce((acc, curr, i) => {
      return [
        ...acc,
        {
          ...curr,
          x: i === 0 ? 0 : acc[i - 1].x + acc[i - 1].width,
          y: j === 0 ? 0 : accRows[j - 1][i].y + accRows[j - 1][i].height
        }
      ];
    }, []);

    return [...accRows, nextRow];
  }, []);

  generatedCells = generatedCells.flat();

  generatedCells = generatedCells.map(({ x, y, width, height }) => ({
    x: x + paddingPx,
    y: y + paddingPx,
    width: width - paddingPx,
    height: height - paddingPx
  }));

  return {
    ...grid,
    width: cols * side,
    height: rows * side,
    cells: generatedCells
  };
}

function getAllPalettes() {
  return chromotome
    .getAll()
    .filter((p) => p.background)
    .map((p) => {
      let { colors } = p;
      const foreground = getForeground(p);
      if (foreground) {
        colors = colors.filter((c) => c !== foreground);
      }
      return { ...p, foreground, colors };
    })
    .filter((p) => p.foreground);
}

function getForeground({
  colors,
  background,
  maxAttempts = 10,
  minContrastRatio = 4.5
} = {}) {
  let attempts = maxAttempts;
  const contrasts = colors
    .map((c) => color.contrastRatio(background, c))
    .map((contrast, index) => ({ contrast, index }));

  const sortedContrast = contrasts
    .slice()
    .sort((a, b) => b.contrast - a.contrast);

  if (sortedContrast[0].contrast > minContrastRatio) {
    return colors[sortedContrast[0].index];
  }
}

/**
 * Computes squares that can be fitted with a rectangle of
 * given width and height.
 *
 * @param      {number}  w       Width of the rectangle
 * @param      {number}  h       Height of the rectangle
 * @param      {number}  n       Approximate number of squares to fit
 * @return     {Object}  {side, rows, cols, count}
 */
function squaresInRectangle(w, h, n) {
  let sx, sy;

  const px = Math.ceil(Math.sqrt((n * w) / h)); //?

  if (Math.floor((px * h) / w) * px < n) {
    sx = h / Math.ceil((px * h) / w);
  } else {
    sx = w / px;
  }

  const py = Math.ceil(Math.sqrt((n * h) / w)); //?

  if (Math.floor((py * w) / h) * py < n) {
    sy = w / Math.ceil((w * py) / h);
  } else {
    sy = h / py;
  }

  const side = Math.max(sx, sy);
  const rows = Math.floor(h / side);
  const cols = Math.floor(w / side);
  const count = rows * cols;

  return {
    side,
    rows,
    cols,
    count
  };
}


/* Set parameters */
let randomize = 47;
const margin = 0.5;
let sampleStrokeWidth = 1.5;
let strokeWidth = 1.5;
const rnd = random.createRandom();
rnd.setSeed(randomize);
let seededRnd = rnd;
const aspectRatio = 4 / 3;
const baseWidth = 960;
const baseHeight = Math.floor(baseWidth / aspectRatio);
const baseGrid = squaresInRectangle(960, 600, 75);
const gridPadding=0.25;
const irGrid = generateIrregularGrid({
  baseGrid,
  random: seededRnd,
  padding: gridPadding
})
const { width, height, cells } = irGrid;
const palette = getPalette(seededRnd);
const size = Math.min(width, height);
const marginPx = (margin * size) / 5;
const canvasWidth = width + 2 * marginPx;
const canvasHeight = height + 2 * marginPx;



const faces = generateFacesData(seededRnd, baseGrid);
console.log(faces);

let number = 1;
for(var i = 0; i < number; i++) {
  const mugshots =  svg`<svg viewBox=${[0, 0, canvasWidth, canvasHeight]}>
    <rect width=${canvasWidth} height=${canvasHeight} fill=${
      palette.background
    }></rect>

    <g transform="translate(${marginPx},${marginPx})">
      ${cells.map((cell, i) => {
        const { x, y, width, height } = cell;
        const { color } = faces[i];
        const fill = palette.colors[color] ?? palette.foreground;
        return svg`<g transform="translate(${x},${y})">
      ${generateFace({
        ...faces[i],
        width,
        height,
        strokeWidth,
        fill,
        stroke: palette.background
      })}
    </g>`;
      })}
    </g>
  </svg>`;


  var container = document.getElementById("container");


  container.innerHTML = mugshots.outerHTML;

  console.log(mugshots);

  download(`catshots_${i}.svg`, mugshots.outerHTML);

  }
